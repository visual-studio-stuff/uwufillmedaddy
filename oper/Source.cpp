#include <iostream>
#include <string>
using std::cout;
using std::endl;
using std::cin;



#define print(x) cout << x << endl

struct Vec2
{
	float x, y;

	Vec2() : x(0.0f), y(0.0f) {}

	Vec2(float X, float Y) : x(X), y(Y) {}

	Vec2 operator+(const Vec2& other) const {
		return Vec2(x + other.x, y + other.y);
	}

	Vec2 operator*(const Vec2& other) const {
		return Vec2(x * other.x, y * other.y);
	}

	bool operator==(const Vec2& other) const {
		return x == other.x && y == other.y;
	}

	bool operator!=(const Vec2& other) const {
		return !(*this == other);
	}

	float distanceTo(const Vec2& other) const {
		return sqrt(pow(other.x - x, 2) + pow(other.y - y, 2));
	}
};

std::ostream& operator<<(std::ostream& stream, const Vec2& other) {
	stream << "x: " << other.x << ", y: " << other.y;
	return stream;
}

namespace time {
	struct Vec2
	{
		float x, t;

		Vec2() : x(0.0f), t(0.0f) {}

		Vec2(float X, float t) : x(X), t(t) {}

		Vec2 operator+(const Vec2& other) const {
			return Vec2(x + other.x, t + other.t);
		}

		Vec2 operator*(const Vec2& other) const {
			return Vec2(x * other.x, t * other.t);
		}

		bool operator==(const Vec2& other) const {
			return x == other.x && t == other.t;
		}

		bool operator!=(const Vec2& other) const {
			return !(*this == other);
		}

		float distanceTo(const Vec2& other) const {
			return other.x - x;
		}
	};
}

int main() {
	Vec2 position(2.0f, 4.0f);
	Vec2 speed(1.0f, 1.0f);
	Vec2 momentum(2.0f, 2.0f);
	time::Vec2 uwu;



	Vec2 t1 = position + speed * momentum;

	cout << t1 << endl;

	cin.get();
}