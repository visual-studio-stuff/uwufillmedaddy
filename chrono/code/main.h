#pragma once
#include <string>
#include <thread>
#include "timer.h"

#define wait(x) std::this_thread::sleep_for(x)

#define block() std::cin.get()

#define print(x) std::cout << x << std::endl;
#define printn(x) std::cout << x << "\n";