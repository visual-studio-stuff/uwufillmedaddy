#pragma once
#include <chrono>
#include <iostream>

class Timer {
public:
	std::chrono::steady_clock::time_point start;
	std::chrono::duration<float> duration;

	Timer() : start(std::chrono::high_resolution_clock::now()) {}

	~Timer() {
		duration = std::chrono::high_resolution_clock::now() - start;
		std::cout << "it took " << duration.count() << " seconds" << std::endl;
	}
};