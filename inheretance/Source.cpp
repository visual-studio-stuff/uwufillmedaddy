#include <iostream>
#include <string>

class Typed {
protected:
	Typed() {}

public:
	virtual std::string type() = 0;
};

class Entity : public Typed {
	std::string type() { return "Entity"; }
};