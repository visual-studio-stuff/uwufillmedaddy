#include <iostream>
#include <string>

class Typed {
protected:

	Typed() {}
public:

	virtual std::string type() = 0;
};

class Entity : public Typed {
public:

	float positionXY[2];

	Entity() : positionXY{ 0, 0 } {}

	Entity(float x, float y) : positionXY{ x , y } {}

	void Move(float xa, float ya) {
		positionXY[0] += xa;
		positionXY[1] += ya;
	}

	std::string type() override { return "Entity"; };
};

class Player : public Entity {
public:
	std::string name;
	int age;

	Player() : name("unnamed"), age(-1) {}

	Player(const std::string& givenName) : name(givenName), age(-1) {}

	Player(int givenAge) : age(givenAge), name("unnamed") {}
	
	Player(std::string givenName, int givenAge) : name(givenName), age(givenAge) {}

	void say(std::string message) {
		std::cout << "[" << name << " : Player]: " << message << std::endl;
	}

	std::string type() override { return "Player"; };
};

int main() {
	//awuga
	std::string awuga = "awoga";

	Player* mainPlayer = new Player("Dario48gay");

	Player awuga = std::string("Dario48gay");

	mainPlayer->say("dw, take yout time, I belive you'll be able to do it :3");

	delete mainPlayer;

	return 0;
}