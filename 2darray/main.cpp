#include <iostream>

#define print(x) std::cout << x << std::endl
#define hold() std::cin.get()

int main() {
	int* array = new int[50];
	int** a2d = new int* [50];

	delete[] array;
	delete[] a2d;

	hold();
}