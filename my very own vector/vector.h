#pragma once
#include <iostream>
namespace MyTypes {
	template <typename type>
	class vector {
		type* content = nullptr;
		int _size = 0;
	public:

		vector(int dimension) {
			content = new type[dimension];
			_size = dimension;
		}

		~vector() {
			delete[] content;
		}

		void add(type what) {
			type* oldcontent = new type[_size + 1];
			for (int i = 0; i < _size; i++) {
				oldcontent[i] = content[i];
			}
			oldcontent[_size] = what;
			delete[] content;
			content = oldcontent;
			_size++;
		}

		template <int dimension>
		void add(type what[dimension]) {
			for (int i = 0; i < dimension; i++) {
				add(what[i]);
			}
		}

		type get(int what) const {
			return content[what];
		}

		int size() const {
			return _size;
		}

		void change(int what, type WithWhat) {
			content[what] = WithWhat;
		}

		void sort() {
			for (int i = 0; i < _size - 1; i++) {
				if (get(i) > get(i + 1)) {
					type temp = get(i);
					change(i, get(i + 1));
					change(i + 1, temp);
				}
			}
			if (!isSorted()) {
				sort();
			}
		}

		bool isSorted() {
			for (int i = 0; i < _size - 1; i++) {
				if (get(i) > get(i + 1)) {
					return false;
				}
			}
			return true;
		}

		void sort(bool (*func)(type, type)) {
			for (int i = 0; i < _size - 1; i++) {
				if (func(get(i), get(i + 1))) {
					type temp = get(i);
					change(i, get(i + 1));
					change(i + 1, temp);
				}
			}
			if (!isSorted(func)) {
				sort(func);
			}
		}

		bool isSorted(bool (*func)(int, int)) {
			for (int i = 0; i < _size - 1; i++) {
				if (func(get(i), get(i + 1))) {
					return false;
				}
			}
			return true;
		}

		friend std::ostream& operator<<(std::ostream& os, vector<type> const& vec) {
			for (int i = 0; i < vec.size(); i++) {
				os << vec.content[i] << ' ';
			}
			return os;
		}
	};
}