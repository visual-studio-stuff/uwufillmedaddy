#include "vector.h"
#include <random>

int main() {
	srand(time(NULL));
	MyTypes::vector<int> unordered = 100;

	for (int i = 0; i < unordered.size(); i++) {
		unordered.change(i, std::rand());
	}

	unordered.sort();
	std::cout << unordered << std::endl;

	unordered.sort([](int a, int b) { return a < b; });
	std::cout << unordered << std::endl;
}