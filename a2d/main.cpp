#include <iostream>

#define print(x) std::cout << x << std::endl
#define hold() std::cin.get()

int main() {
	int* array = new int[50];
	int** a2d = new int* [50];

	for (int i = 0; i < 50; i++) {
		a2d[i] = new int[50];
	}

	delete[] array;
	for (int i = 0; i < 50; i++)
		delete[] a2d[i];
	delete[] a2d;

	hold();
}